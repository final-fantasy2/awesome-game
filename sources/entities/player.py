import arcade.key

from .base_creature import BaseCreature
from sources.config.player_constants import PlayerConstants as pc


class Player(BaseCreature):
    def __init__(self, position_x, position_y, attributes):
        super().__init__()
        self.position_x = position_x
        self.position_y = position_y
        self.attributes = attributes
        self.change_x = 0
        self.change_y = 0
        self.sprite_list = arcade.SpriteList()
        self.player_sprite = arcade.Sprite(pc.SPRITES_LOCATION + "/character.png")
        self.player_sprite.center_x = position_x
        self.player_sprite.center_y = position_y
        self.sprite_list.append(self.player_sprite)

    def update(self):
        self.position_x += self.change_x
        self.player_sprite.center_x = self.position_x
        self.player_sprite.center_y = self.position_y

    def draw(self):
        self.sprite_list.draw()

    def move(self, key):
        if key == arcade.key.LEFT:
            self.change_x = -pc.PLAYER_SPEED_X
        if key == arcade.key.RIGHT:
            self.change_x = pc.PLAYER_SPEED_X

    def key_released(self, key):
        if key == arcade.key.LEFT:
            self.change_x = 0
        if key == arcade.key.RIGHT:
            self.change_x = 0
