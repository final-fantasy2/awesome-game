from abc import ABC, abstractmethod


class BaseCreature(ABC):
    def __init__(self):
        pass

    @abstractmethod
    def move(self, key):
        pass

    @abstractmethod
    def update(self):
        pass

    @abstractmethod
    def draw(self):
        pass
