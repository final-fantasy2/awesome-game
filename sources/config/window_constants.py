class WindowConstants:
    SCREEN_WIDTH: int = 800
    SCREEN_HEIGHT: int = 600
    SCREEN_TITLE: str = "Awesome game"
    RESIZABLE: bool = False
