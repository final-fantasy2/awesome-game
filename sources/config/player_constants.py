import os


class PlayerConstants:
    SPRITES_LOCATION: str = os.getcwd() + "../../resources/player_assets"
    PLAYER_POSITION_X: int = 50
    PLAYER_POSITION_Y: int = 50
    PLAYER_ATTRIBUTES: dict = {}
    PLAYER_SPEED_X: int = 10
    PLAYER_SPRITE_SCALING: float = 0.5
