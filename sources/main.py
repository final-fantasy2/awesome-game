import arcade

from game_window import Window
from config.window_constants import WindowConstants


def main():
    window = Window(WindowConstants.SCREEN_WIDTH, WindowConstants.SCREEN_HEIGHT,
                    WindowConstants.SCREEN_TITLE, WindowConstants.RESIZABLE)
    arcade.run()


if __name__ == '__main__':
    main()
