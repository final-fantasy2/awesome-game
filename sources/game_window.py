import arcade

from entities.player import Player


class Window(arcade.Window):
    # Конструктор кастомного окна
    def __init__(self, screen_width, screen_height, screen_title, resizable):
        super().__init__(screen_width, screen_height,
                         screen_title, resizable)

        self.tile_map = None

        self.scene = None

        self.player = Player(100, 100, None)

        arcade.set_background_color(arcade.color.ASH_GREY)

        self.set_mouse_visible(False)

    # Вызывается на каждой итерации игрового цикла
    def on_draw(self):
        arcade.start_render()
        self.player.draw()

    # Вызывается для обновления объектов
    def on_update(self, delta_time):
        self.player.update()

    def on_key_press(self, key: int, modifiers: int):
        self.player.move(key)

    def on_key_release(self, key: int, modifiers: int):
        self.player.key_released(key)
